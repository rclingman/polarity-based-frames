from polarity import *

    
### WORKING EXAMPLES ###
a,b,c,d = "a", "b", "c", "d"

# Rel1 = {(1,2),(1,3), (2,1),(2,3),(3,1),(3,2)}
# Rel2 = {(1,2),(1,3), (2,1),(2,3)}
# A1 = {1,2}
# X1 = {1,2,3}                      
# pol = Polarity(A1,X1,Rel2)
# pol2 = Polarity(A1,X1,Rel2)

# A2 = {1,2,3}
# X2 = {a,b,c}
# Rel3 = {(1,a),(1,b),(1,c),(2,b),(2,c),(3,c)}
# pol3 = Polarity(A2,X2,Rel3)

# Example 3.8

W1 = {a,b,c,d}
W2 = {1,2,3}
Rel1 = {(a,b),(a,c),(b,d),(c,d)}
Rel2 = {(1,2),(2,3)}
K1 = KripkeFrame(W1,Rel1)
K2 = KripkeFrame(W2,Rel2)
F1 = EnrichedPolarity.kripke_lifting(K1)
F2 = EnrichedPolarity.kripke_lifting(K2)

S = BinaryRelation(W1,W2,{(a,2),(a,3),(b,1),(b,3),(c,1),(c,3),(d,1),(d,2)})

# Example 3.3

# F3 = {a,b}
# F4 = {1}
# P3 = Polarity.set_lifting(F3)
# P4 = Polarity.set_lifting(F4)
# R = set()
# S = BinaryRelation(P3.objs,P4.attrs,R)
# T = BinaryRelation(P3.attrs,P4.objs,R)
# print(P3.is_stable_ext(set()))

# Testing P2-P4 in all 2 - 2 polarities

# A1 = {a,b}
# X1 = {a,b}
# A2 = {1}
# X2 = {1}
                    
# possibleI1 = powerset(cross_ops(A1,X1)) 
# possibleI2 = powerset(cross_ops(A2,X2))
# possible_S = powerset(cross_ops(A1,X2))
# possible_T = powerset(cross_ops(X1,A2))
# p_morphic = []
# not_p_morphic = []
# X = {a,b,c,d}
# X = powerset(X)


# K = KripkeFrame({1,2},{(1,2),(2,2)})


# for I1 in possibleI1:
#     P1 = Polarity(A1,X1,I1)
#     for I2 in possibleI2:
#         P2 = Polarity(A2,X2,I2)
#         for s in possible_S:
#             S = BinaryRelation(A1,X2,s)
#             for t in possible_T:
#                 T = BinaryRelation(X1,A2,t)                
#                 axioms = [check_p2(P1,P2,S,T),check_p3(P1,P2,S,T),check_p4(P1,P2,S,T)]
#                 if axioms[0] == False:
#                     print((I1,I2,s,t))

#print(len(p_morphic))

# for s in possible_ST:
#     S = BinaryRelation(F1,F2,s)
#     for t in possible_ST:
#         T = BinaryRelation(F1,F2,t)
#         axioms = [check_p2(P1,P2,S,T),check_p3(P1,P2,S,T),check_p4(P1,P2,S,T)]
#         if axioms == [True,True,True]:
#             p_morphs.append((s,t))
#         else:
#             not_p_morphs.append((s,t,axioms))
        
# print(len(possible_ST) ** 2)
# print(len(p_morphs))
# for a in not_p_morphs:
#     if a[2] != [True,True,False]:
#         print(a)

#print(check_p4(P3,P4,,ST))

# New example


# W1 = {a,b,c}
# R1 ={(b,c)}
# W2 = {1,2}
# R2 = {(1,2)}

# K1 = KripkeFrame(W1,R1)
# K2 = KripkeFrame(W2,R2)

# F1 = EnrichedPolarity.kripke_lifting(K1)
# F2 = EnrichedPolarity.kripke_lifting(K2)


# def get_axioms(frame1,frame2,S,T):
#     axioms = [check_p2(F1,F2,S,T),check_p3(F1,F2,S,T),check_p4(F1,F2,S,T),check_p5(F1,F2,S,T)]
#     return axioms
    

# for S in all_binary_relations(F1.objs,F2.attrs):
#     for T in all_binary_relations(F1.attrs,F2.objs):        
#         axioms = [check_p2(F1,F2,S,T),check_p3(F1,F2,S,T),check_p4(F1,F2,S,T),check_p5(F1,F2,S,T)]
#         if axioms == [True,True,True,True,True]:
#             print(S.ops)
#             print("S: " + str(S.ops))
#             print("T: " + str(T.ops))

# def check_p5(F1,F2,S,T):
#     rel1 = F1.add_relations[0]
#     rel2 = F2.add_relations[0]    
#     p5_satisfied = True
#     # checks 5.2 and 5.3
#     for a2 in F2.objs:
#         vals = []
#         rhs = rel1.back(F1.down(T.back({a2}))) # 5.2 and 5.3 have some RHS
#         vals.append((S.back(F2.up(rel2.back({a2}))) != rhs))
#         vals.append((T.back(F2.down(rel2.back({a2}))) != rhs))
#         if S.back(F2.up(rel2.back({a2}))) != rhs or T.back(F2.down(rel2.back({a2}))) != rhs: # 5.3 and 5.3
#             p5_satisfied = False
#     print(vals)
#     # checks 5.1 and 5.4 if 5.2 and 5.3 are satisfied    
#     if (p5_satisfied) is True:
#         for x2 in F2.attrs:
#             rhs = rel1.back(F1.up(S.back({x2})))
#             if S.back(F2.up(rel2.back({x2}))) != rhs or T.back(F2.down(rel2.back({x2}))) != rhs:
#                 p5_satisfied = False
#     return p5_satisfied
            
# 2021-03-30: Trying to find some b's such that S^0(b) = a for different examples to get the injective version of the theorem.

# W1 = {a,b}
# W2 = {1,2,3}

# f = {a:2,
#      b:3}

# F = Mapping(W1,f)

# print("")
# for subset in powerset(W1):
#     for subset2 in powerset(W2):
#         if (W1 - F.preimage(subset2)) == subset:
#             print("(f^{-1} [" + str(subset2) + "])^c = " + str(subset))

