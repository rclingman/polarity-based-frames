"""Set Theory Functions"""

def powerset(Set):
    """Returns the powerset of Set as a list of sets."""
    power_list = [[]] # algorithm adapted from https://rosettacode.org/wiki/Power_set#Python
    for element in list(Set):
        power_list.extend([subset + [element] for subset in power_list])
    power_set_list = [set()] 
    for subset in power_list: # conversion of list of lists to list of sets 
        if subset != []:
            power_set_list.append(set(subset))
    return(power_set_list)

""" 
GENERATING ORDERED PAIRS


The functions in this section are used for generating lists of ordered pairs from sets. Functions such as compliment_of_ops return ordered pair lists that are later used in related BinRelation class methods such as compliment. These functions are external to class, as they can be useful as they may be useful as stand alone functions.

Note: We use the abbreviation, ops, for ordered pairs throughout this project.
"""

def cross_ops(Set1,Set2):
    """Return set of ordered pairs corresponding to the cross product Set1 x Set2."""
    relation = []
    for element1 in Set1:
        for element2 in Set2:
            relation.append((element1,element2))
    return set(relation)

def compliment_of_ops(Set1,Set2,ops):
    """Returns the set of ordered pairs cross_ops(Set1,Set2) - ops (the set compliment of ops)"""
    # TODO: ValueError for when ops is unrelated to Set1 and Set2    
    return cross_ops(Set1,Set2) - ops

def set_id_ops(Set1):
    """Returns set of ordered pairs corresponding to the identity relation {(a,a) : a in Set} for input Set."""
    return {(a,a) for a in Set1}

def set_id_compliment(Set1):
    """Returns the set of ordered pairs corresponding to the compliment to the identity relation {(a,x) : a =/= x with a,x in Set}"""
    return compliment_of_ops(Set1,Set1,set_id_ops(Set1))

def all_ops(Set1,Set2):
    """Generates list of all sets of ordered pairs in power set of Set1 x Set2"""
    return list(powerset(cross_ops(Set1,Set2)))

class BinaryRelation(object):
    """ Binary relation between sets.
        Arguments: 
              domain: set object
              codomain: set object
              ordered_pairs: set of pairs (a,x) where a is in domain and x is in codomain
    """
    def __init__(self,domain,codomain,ordered_pairs):
        """Constructs BinaryRelation object from domain, codomain, ordered_pairs
               domain (set): domain of binary relation
               codomain (set): codomain of binary relation
               ordered_pairs (set): set of ordered pairs from domain x codomain.
        """
        self.domain = domain
        self.codomain = codomain
        self.ops = ordered_pairs

    """The up and down relations on a polarity are generalizations of what we will call the forward and back operations on binary relation S < A x X."""

    def _forward_singleton(self,element):
        """Forward operation of Relation < Set1 x Set2 for given singleton 'element'."""
        if element not in self.domain:
            raise ValueError("%s not in %s." % (str(element),str(self.domain)))
        # If the relation is empty (ordered_pairs = set()) then forward({element}) = codomain, for any {element} in domain.
        elif self.ops == set():
            return self.codomain
        else:
            # forward_list: list of elements related to element by BinaryRelation.
            forward_list = []
            for pair in self.ops:
                if pair[0] == element:
                    forward_list.append(pair[1])
            return set(forward_list)

    def _back_singleton(self,element):
        """Back operation of Relation < Set1 x Set2 for a singleton 'element'."""
        if element not in self.codomain:
            raise ValueError("%s not in %s." % (str(element),str(self.codomain)))
        # If the relation is empty (ordered_pairs = set()) then back({element}) = domain, for any {element} in codomain. 
        elif self.ops == set():
            return self.domain
        else:
            back_list = []
            for pair in self.ops:
                if pair[1] == element:
                    back_list.append(pair[0])
            return set(back_list)
    
    def forward(self,subset):
        """Returns the forward set of a subset of Set1 given Relation < Set1 x Set2. For binary relation  
           $S \subset A \timex X$ and subset $U \subset A$ this is often written $S^{(1)} (U)$."""
        if subset == set():
            return self.codomain
        elif (subset <= self.domain) == False:
            raise ValueError("Set %s not a subset of %s." % (str(subset),str(self.domain)))
        elif self.ops == set():
            # case when the subset is non-empty and the relation is empty -- this always gives the empty set. 
            return set()
        else:
            # The forward set of any non-empty subset is just the intersection of the forward sets of its singletons.
            forward_singletons = [self._forward_singleton(element) for element in subset]
            # Handling for when forward_singletons is empty."""
            if forward_singletons == []:
                forward_subset = set()
            else:
                forward_subset = set.intersection(*forward_singletons)
            return forward_subset

    def back(self,subset):
        """Returns the back set of a subset of Set2 given Relation < Set1 x Set2. For binary relation  
        $S \subset A \timex X$ and subset $V \subset X$ this is often written $S^{(0)} (V)$."""
        # The back set of the empty set is Set1. This seems to be the best way of handling the vacous case, unfortunately.
        if subset == set():
            return self.domain
        elif (subset <= self.codomain) == False:
            raise ValueError("Set %s not a subset of %s." % (str(subset),str(self.codomain)))
        elif self.ops == set():
            # case when subset is non-empty and the relation is empty -- this always gives the empty set. 
            return set()
        else:
            # Handling for when back_singletons is empty.
            back_singletons = [self._back_singleton(element) for element in subset]
            if back_singletons == []:
                back_subset = set()
            else:
                back_subset = set.intersection(*back_singletons)
            return back_subset

    def compliment(self):
        """Generates the compliment of a binary Relation < Set1 x Set2"""
        return BinaryRelation(self.domain,self.codomain,compliment_of_ops(self.domain,self.codomain,self.ops))

    def to_string(self):
        """Returns binary relation as string of form (domain,codomain,ops)"""
        return(str((self.domain,self.codomain,self.ops)))
    
def all_binary_relations(Set1,Set2):
    """Generates a list of all BinaryRelation objects between Set1 and Set2"""
    all_binary_relations = []
    for ops in all_ops(Set1,Set2):
        all_binary_relations.append(BinaryRelation(Set1,Set2,ops))
    return all_binary_relations


class Polarity(BinaryRelation):

    """ A polarity object. Mathematically, P = (A,X,I) where A,X are non-empty sets and I is a binary relation between A and X.
            Arguments: 
              objects: set object
              attributes: set object
              incidence_ops: set of pairs (a,x) where a is in objects and x is in attributes.
    """
    def __init__(self,objects,attributes,incidence_ops):
        """Creates polarity object from objects (set), attributes (set), and incidence_ops (list of ordered pairs). Endowed with methods and attributes from BinaryRelation class."""
        self.objs = objects
        self.attrs = attributes
        self.I = incidence_ops
        # inherit BinaryRelation class methods and attributes
        super().__init__(objects,attributes,incidence_ops)
        
    @classmethod
    def from_binary_relation(cls,binary_relation):
        """Create polarity object from BinaryRelation object."""
        return cls(binary_relation.domain,binary_relation.codomain,binary_relation.ops)

    @classmethod
    def set_lifting(cls,set1):
        """Create polarity object corresponding to lifting of set1 in the sense of Definition 3.3 of https://arxiv.org/pdf/1907.00359.pdf"""
        return cls(set1,set1,set_id_compliment(set1))
    
    def up(self,subset):
        """Returns the intention of subset. This is just the forward set of the subset under the I relation of the polarity."""
        return self.forward(subset)

    def down(self,subset):
        """Returns the extension of a subset. This is the back set of the subset under the I relation."""
        return self.back(subset)

    def updown(self,subset):
        """Returns the updown (closure) of subset."""
        return self.down(self.up(subset))

    def downup(self,subset):
        """Returns the downup (closure) of subset."""
        return self.up(self.down(subset))
        
    def is_stable_ext(self,subset):
        """Returns True if extension subset is Galois stable, and False else."""
        if self.down(self.up(subset)) == subset:
            return True
        else:
            return False
    
    def is_stable_int(self,subset):
        """Returns False if intension subset is Galois stable, and False otherwise."""
        if self.up(self.down(subset)) == subset:
            return True
        else:
            return False

    ### ALIASES ####
    intention = up
    extension = down

class Mapping(object):
    """ Mapping object consists of a domain set, A, and a dictionary consisting of keys from the domain and values from the codomain
        Arguments:
            domain: set
            f: dictionary
    """

    def __init__(self,domain,f):
        self.f = f
        self.domain = domain
        self.codomain = set(f.values())

    def preimage(self,subset):
        """Returns subsets preimage under mapping
        Arguments: 
                subset: set
        """
        preimage = set()
        for element in self.domain:
            if element in self.f.keys() and self.f[element] in subset: 
                preimage.add(element)
        return preimage

    def image(self,subset):
        """Returns subset's image under mapping
        Arguments:
                 subset: set
        """
        image = set()
        for element in set.intersection(subset,self.domain):
            image.add(self.f[element])
        return image
        
class KripkeFrame(object):
    """ Kripke Frame object consisting of set of possible worlds, W, and an accessibility relation 
            Arguments: 
              W: Set of states
              accessibility_relation: set of ordered pairs from W x W
    Question: Should this take in a BinaryRelation object, or just the ops?
    """

    def __init__(self,W,accessiblity_relation):
        self.W = W        
        self.R = accessiblity_relation
        for op in self.R:
            if (op[0] not in self.W) or (op[1] not in self.W):
                raise ValueError("Invalid ordered pair in accessibility relation: %s not in %s x %s." % (str(op),str(self.W),str(self.W)))


    def is_accessible(self,state1,state2):
        """Returns True if (state1,state2) is in relation_ops (if they are related in the Frame) and False if not."""
        if state1 not in self.W:
            raise ValueError("%s not in %s" % (str(state1),str(self.W)))
        elif state2 not in self.W:
            raise ValueError("%s not in %s" % (str(state2),str(self.W)))
        else:
            if (state1,state2) in self.R:
                return(True)
            else:
                return(False)

    def access_states(self,state):
        """Returns set of accessible states from input state."""
        accessible_states = set()
        for pair in self.R:
            if self.is_accessible(state,pair[1]) is True:
                accessible_states.add(pair[1])
        return(accessible_states)

class EnrichedPolarity(Polarity):
    """Enriched polarity object is a polarity along with an iterable of additional_relations on the polarity.
            Arguments: 
              polarity: polarity object 
              additional_relations: set, tuple, or list binary relations on the polarity (subsets of A x A, X x X, A x X, or X x A)
    """

    def __init__(self,polarity,additional_relations):
        """"Creates EnrichedPolarity object from polarity and tuple of additional relations.
        TODO: Possibly add check to see if additional relations are in the polarity. May not be worth the computing time. 
        """ 
        self.polarity = polarity
        self.add_relations = additional_relations
        super().__init__(polarity.objs,polarity.attrs,polarity.I)         # inherit Polarity class methods and attributes

    @classmethod
    def kripke_lifting(cls,kripke_frame):
        """Generates an enriched polarity object from a Kripke Frame in the sense of 3.3 of https://arxiv.org/pdf/1907.00359.pdf"""
        polarity = Polarity.set_lifting(kripke_frame.W)
        compliment_relation = BinaryRelation(kripke_frame.W,kripke_frame.W,compliment_of_ops(kripke_frame.W,kripke_frame.W,kripke_frame.R))
        # Question: The binary relation object for each type of relation of a KF lifting will be represented the same in our code. Is this a problem? 
        return cls(polarity,[compliment_relation])
          
            
#     ### Checking GT Axioms #### 

# def check_p2(pol_1,pol_2,S,T):
#     p2_satisfied = True
#     for a1 in pol_1.objs:
#        if (pol_2.is_stable_int(S.forward({a1}))) is False:
#            p2_satisfied = False
#     if p2_satisfied is True:
#         for x1 in pol_1.attrs:
#             if (pol_2.is_stable_ext(T.forward({x1}))) is False:
#                 p2_satisfied = False
#     if p2_satisfied is True:
#         for a2 in pol_2.attrs:
#             if (pol_1.is_stable_int(T.back({a2}))) is False:
#                 p2_satisfied = False
#     if p2_satisfied is True:
#         for x2 in pol_2.objs:
#             if (pol_1.is_stable_ext(S.back({x2}))) is False:
#                 p2_satisfied = False
#     return p2_satisfied
                    
# def check_p3(pol_1,pol_2,S,T):
#     p3_satisfied = True
#     for a2 in pol_2.objs:
#         set1 = pol_1.down(T.back({a2}))
#         set2 = S.back(pol_2.up({a2}))
#         if (set1 <= set2) is False:
#             p3_satisfied = False
#     return p3_satisfied

# def check_p4(pol_1,pol_2,S,T):
#     p4_satisfied = True    
#     for a1 in pol_1.objs:
#         set1 = T.back(pol_2.down(S.forward({a1})))
#         set2 = pol_1.up({a1})
#         if (set1 <= set2) is False:
#             p4_satisfied = False
#             break
#     return p4_satisfied

# def check_p5(F1,F2,S,T):
#     rel1 = F1.add_relations[0]
#     rel2 = F2.add_relations[0]    
#     p5_satisfied = True
#     p51_satisfied, p52_satisfied, p53_satisfied, p54_satisfied = True,True,True,True
#     # checks 5.2 and 5.3
#     for a2 in F2.objs:
#         rhs = rel1.back(F1.down(T.back({a2}))) # 5.2 and 5.3 have some RHS
#         if S.back(F2.up(rel2.back({a2}))) != rhs:
#             p51_satisfied = False
#         if  T.back(F2.down(rel2.back({a2}))) != rhs:
#             p52_satisfied = False
#     for x2 in F2.attrs:
#         rhs = rel1.back(F1.up(S.back({x2})))
#         if S.back(F2.up(rel2.back({x2}))) != rhs:
#             p53_satisfied = False
#         if T.back(F2.down(rel2.back({x2}))) != rhs:
#             p54_satisfied = False
#     return [p51_satisfied,p52_satisfied,p53_satisfied,p54_satisfied]

# def print_p5(F1,F2,S,T):
#     rel1 = F1.add_relations[0]
#     rel2 = F2.add_relations[0]    
#     print("For 5.1")
#     print("-------")
#     for x2 in F2.attrs:
#         workout_string = """
#         x2 = L1
#         LHS: S.back(F2.up(rel2.back({L1})))))
#              S.back(F2.up(L2))
#              S.back(L3)
#              L4
#         RHS: rel1.back(F1.up(S.back({R1})))
#              rel1.back(F1.up(R2))
#              rel1.back(R3)
#              R4
#         """
#         replace_terms = {'L1': str(x2),
#                  'L2': str(rel2.back({x2})),
#                  'L3': str(F2.up(rel2.back({x2}))),
#                  'L4': str(S.back(F2.up(rel2.back({x2})))),
#                  'R1': str(x2),
#                  'R2': str(S.back({x2})),
#                  'R3': str(F1.up(S.back({x2}))),
#                  'R4': str(rel1.back(F1.up(S.back({x2}))))
#                          }
#         for key,value in replace_terms.items():
#             workout_string = workout_string.replace(key,value)
#         print(workout_string)        
#     print("For 5.2")
#     print("-------")
#     for a2 in F2.objs:
#         workout_string = """
#         a2 = L1
#         LHS: T.back(F2.down(rel2.back({L1})))))
#              T.back(F2.down(L2))
#              T.back(L3)
#              L4
#         RHS: rel1.back(F1.down(T.back({R1})))
#              rel1.back(F1.down(R2))
#              rel1.back(R3)
#              R4
#         """
#         replace_terms = {'L1': str(a2),
#                  'L2': str(rel2.back({a2})),
#                  'L3': str(F2.down(rel2.back({a2}))),
#                  'L4': str(T.back(F2.down(rel2.back({a2})))),
#                  'R1': str(a2),
#                  'R2': str(T.back({a2})),
#                  'R3': str(F1.down(T.back({a2}))),
#                  'R4': str(rel1.back(F1.down(T.back({a2}))))
#                          }
#         for key,value in replace_terms.items():
#             workout_string = workout_string.replace(key,value)
#         print(workout_string)        
#     print("For 5.3")
#     print("-------")
#     for a2 in F2.objs:
#         workout_string = """
#         a2 = L1
#         LHS: S.back(F2.up(rel2.back({L1})))))
#              S.back(F2.up(L2))
#              S.back(L3)
#              L4
#         RHS: rel1.back(F1.down(T.back({R1})))
#              rel1.back(F1.down(R2))
#              rel1.back(R3)
#              R4
#         """
#         replace_terms = {'L1': str(a2),
#                  'L2': str(rel2.back({a2})),
#                  'L3': str(F2.up(rel2.back({a2}))),
#                  'L4': str(S.back(F2.up(rel2.back({a2})))),
#                  'R1': str(a2),
#                  'R2': str(T.back({a2})),
#                  'R3': str(F1.down(T.back({a2}))),
#                  'R4': str(rel1.back(F1.down(T.back({a2}))))
#                          }
#         for key,value in replace_terms.items():
#             workout_string = workout_string.replace(key,value)
#         print(workout_string)        
#     print("For 5.4")
#     print("-------")
#     for x2 in F2.attrs:
#         workout_string = """
#         x2 = L1
#         LHS: T.back(F2.down(rel2.back({L1})))))
#              T.back(F2.down(L2))
#              T.back(L3)
#              L4
#         RHS: rel1.back(F1.up(S.back({R1})))
#              rel1.back(F1.up(R2))
#              rel1.back(R3)
#              R4
#         """
#         replace_terms = {'L1': str(x2),
#                  'L2': str(rel2.back({x2})),
#                  'L3': str(F2.down(rel2.back({x2}))),
#                  'L4': str(T.back(F2.down(rel2.back({x2})))),
#                  'R1': str(x2),
#                  'R2': str(S.back({x2})),
#                  'R3': str(F1.up(S.back({x2}))),
#                  'R4': str(rel1.back(F1.up(S.back({x2}))))
#                          }
#         for key,value in replace_terms.items():
#             workout_string = workout_string.replace(key,value)
#         print(workout_string)        
 

a,b,c,d,x,y,z = "a", "b", "c", "d", "x", "y", "z"


