## Checking adjunction relation for box and black diamond

# for pol in polarities:
#     ops = all_ops(pol.objs,pol.attrs)    
#     ops.remove(set())
#     for rel in ops:
#         Rbox = BinaryRelation(pol.objs,pol.attrs,rel)
#         for gamma_val in get_valuation_extents(pol):
#             for psi_val in get_valuation_extents(pol):
# # A < B iff C < D is false when (A < B and C </ D) or (C < D and A </B)                
#                 psi_descr = pol.up(psi_val)
#                 box_psi_val = pol.down(pol.up(Rbox.back(psi_descr)))
#                 bl_diamond_gamma_val = pol.down(Rbox.forward(gamma_val))
#                 condition_1 = (gamma_val <= box_psi_val)
#                 condition_2 = (bl_diamond_gamma_val <= psi_val)
#                 if ((condition_1 == True) and (condition_2 == False)) or ((condition_1 == False) and (condition_2 == True)):
#                     P_str = pol.to_string()
#                     R_str = Rbox.to_string()
#                     gamma_str = str(gamma_val)
#                     psi_str = str(psi_val)
#                     if pol.ops != {('a', 'y'), ('b', 'x'), ('a', 'x'), ('b', 'y')}:
#                         print("P: " + P_str)
#                         print("Rbox: " + R_str)
#                         print("gamma: " + gamma_str)
#                         print("psi: " + psi_str)

# # P: ({'b'}, {'x', 'y'}, {('b', 'x')})
# # Rbox: ({'b'}, {'x', 'y'}, {('b', 'y')})
# # gamma: {'b'}
# # psi: {'b'}
                
                    
## Checking adjunction relations for diamond and black box

# for pol in polarities:
#     ops = all_ops(pol.attrs,pol.objs)    
#     ops.remove(set())
#     for rel in ops:
#         Rd = BinaryRelation(pol.attrs,pol.objs,rel)
#         for gamma_val in get_valuation_extents(pol):
#             for psi_val in get_valuation_extents(pol):
# # A < B iff C < D is false when (A < B and C </ D) or (C < D and A </B)                
#                 psi_descr = pol.up(psi_val)
#                 dia_gamma_val = pol.down(Rd.back(gamma_val))
#                 bl_box_psi_val = pol.down(pol.up(Rd.forward(psi_descr)))
#                 condition_1 = (dia_gamma_val <= psi_val)
#                 condition_2 = (gamma_val <= bl_box_psi_val)
#                 if ((condition_1 == True) and (condition_2 == False)) or ((condition_1 == False) and (condition_2 == True)):
#                     P_str = pol.to_string()
#                     R_str = Rd.to_string()
#                     gamma_str = str(gamma_val)
#                     psi_str = str(psi_val)
#                     print("P: " + P_str)
#                     print("Rbox: " + R_str)
#                     print("gamma: " + gamma_str)
#                     print("psi: " + psi_str)

## Check adjunction properties for lhd and blacklhd

# for pol in polarities:
#     ops = all_ops(pol.attrs,pol.attrs)    
#     ops.remove(set())
#     for rel in ops:
#         Rlhd = BinaryRelation(pol.attrs,pol.attrs,rel)
#         for gamma_val in get_valuation_extents(pol):
#             for psi_val in get_valuation_extents(pol):
# # A < B iff C < D is false when (A < B and C </ D) or (C < D and A </B)                
#                 psi_descr = pol.up(psi_val)
#                 gamma_descr = pol.up(gamma_val)
#                 lhd_gamma_back = pol.down(Rlhd.back(gamma_descr))
#                 lhd_psi_forward = pol.down(Rlhd.forward(psi_descr))
#                 condition_1 = (lhd_gamma_back <= psi_val)
#                 condition_2 = (lhd_psi_forward <= gamma_val)
#                 if ((condition_1 == True) and (condition_2 == False)) or ((condition_1 == False) and (condition_2 == True)):
#                     if (pol.objs == {b}) and (pol.attrs == {x,y}) and (gamma_val == {b}) and (psi_val == {b}):
#                         P_str = pol.to_string()
#                         R_str = Rlhd.to_string()
#                         gamma_str = str(gamma_val)
#                         psi_str = str(psi_val)
#                         print("P: " + P_str)
#                         print("Rlhd: " + R_str)
#                         print("gamma: " + gamma_str)
#                         print("psi: " + psi_str)

## Check adjunction properties for rhd and black_rhd

# for pol in polarities:
#     ops = all_ops(pol.objs,pol.objs)    
#     ops.remove(set())
#     for rel in ops:
#         Rrhd = BinaryRelation(pol.objs,pol.objs,rel)
#         for gamma_val in get_valuation_extents(pol):
#             for psi_val in get_valuation_extents(pol):
# # A < B iff C < D is false when (A < B and C </ D) or (C < D and A </B)                
#                 psi_descr = pol.up(psi_val)
#                 gamma_descr = pol.up(gamma_val)
#                 rhd_psi_back = pol.down(pol.up(Rrhd.back(psi_val)))
#                 rhd_gamma_forward = pol.down(pol.up(Rrhd.forward(gamma_val)))
#                 condition_1 = (gamma_val <= rhd_psi_back)
#                 condition_2 = (psi_val <= rhd_gamma_forward)
#                 if ((condition_1 == True) and (condition_2 == False)) or ((condition_1 == False) and (condition_2 == True)):
#                     P_str = pol.to_string()
#                     R_str = Rrhd.to_string()
#                     gamma_str = str(gamma_val)
#                     psi_str = str(psi_val)
#                     print("P: " + P_str)
#                     print("Rlhd: " + R_str)
#                     print("gamma: " + gamma_str)
#                     print("psi: " + psi_str)
