from polarity import *

a,b,c,d,w,x,y,z = "a", "b", "c", "d","w", "x", "y", "z"

object_set = {a,b,c}
feature_set = {x,y,z}

object_sets = []
for subset in powerset(object_set):
    if subset != set():
        object_sets.append(subset)

feature_sets = []
for subset in powerset(feature_set):
    if subset != set():
        feature_sets.append(subset)

# Build all polarities from objects and feature sets
polarities = []

for A in object_sets:
    for X in feature_sets:
        ops = all_ops(A,X)
        ops.remove(set())
        for I in ops:
            P = Polarity(A,X,I)
            polarities.append(P)

# Gets exts for all concepts of P

def get_valuation_extents(P):
    vals = []
    for subset in powerset(P.objs):
        if subset == P.down(P.up(subset)):
            vals.append(subset)
    return(vals)

def rel_eq(rel1,rel2):
    if rel1.ops == rel2.ops:
        return(True)
    else:
        return(False)

## Distribution of box check

# for pol in polarities:
#     ops = all_ops(pol.objs,pol.attrs)    
#     ops.remove(set())
#     for rel in ops:
#         Rbox = BinaryRelation(pol.objs,pol.attrs,rel)        
#         for p_val in get_valuation_extents(pol):
#             p_descr = pol.up(p_val)
#             for q_val in get_valuation_extents(pol):
#                 q_descr = pol.up(q_val)
#                 rhs = pol.updown(Rbox.back(pol.up(p_val.intersection(q_val))))
#                 lhs = pol.updown(Rbox.back(p_descr)).intersection(pol.updown(Rbox.back(q_descr)))
#                 # if rel_eq(Rbox,pol) == False:
#                 #     print("P:" + pol.to_string())
#                 #     print("Rbox:" + Rbox.to_string())
#                 #     print("p:" + str(p_val))
#                 #     print("q:" + str(q_val))
#                 #     print("lhs:" + str(lhs))
#                 #     print("rhs:" + str(rhs))
        
#                 if (lhs <= rhs) == False:
#                     P_str = pol.to_string()
#                     R_str = Rbox.to_string()
#                     p_str = str(p_val)
#                     q_str = str(q_val)
#                     print("P: " + P_str)
#                     print("Rbox: " + R_str)
#                     print("p_val: " + p_str)
#                     print("q_val: " + q_str)
                

## Distribution of lhd check with polarity we chose in box distribution example
# polarities = [Polarity({a,b},{x,y,z},{(a,x),(b,z)})]
              
# for pol in polarities:
#     ops = all_ops(pol.attrs,pol.attrs)    
#     ops.remove(set())
#     for rel in ops:
#         Rlhd = BinaryRelation(pol.attrs,pol.attrs,rel)        
#         p_val = {a}
#         q_val = {b}
#         p_descr = pol.up(p_val)
#         q_descr = pol.up(q_val)
#         rhs = pol.downup(Rlhd.back(p_descr)).intersection(pol.downup(Rlhd.back(q_descr)))
#         lhs = pol.downup(Rlhd.back(pol.up(q_val.intersection(p_val))))
#         if (lhs >= rhs) == False and len(pol.ops) < 3:                
#             P_str = pol.to_string()
#             R_str = Rlhd.to_string()
#             p_str = str(p_val)
#             q_str = str(q_val)
#             print("P: " + P_str)
#             print("Rlhd: " + R_str)
#             print("p_val: " + p_str)
#             print("q_val: " + q_str)

# Distribution of lhd check - complete search space
#polarities = [Polarity({a,b},{x,y,z},{(a,x),(b,z)})]
              
# for pol in polarities:
#     ops = all_ops(pol.attrs,pol.attrs)    
#     ops.remove(set())
#     for rel in ops:
#         Rlhd = BinaryRelation(pol.attrs,pol.attrs,rel)        
#         for p_val in get_valuation_extents(pol):
#             p_descr = pol.up(p_val)
#             for q_val in get_valuation_extents(pol):
#                 q_descr = pol.up(q_val)
#                 rhs = pol.downup(Rlhd.back(p_descr)).intersection(pol.downup(Rlhd.back(q_descr)))
#                 lhs = pol.downup(Rlhd.back(pol.up(q_val.intersection(p_val))))
#                 if (lhs >= rhs) == False and len(pol.ops) < 3:                
#                     P_str = pol.to_string()
#                     R_str = Rlhd.to_string()
#                     p_str = str(p_val)
#                     q_str = str(q_val)
#                     print("P: " + P_str)
#                     print("Rlhd: " + R_str)
#                     print("p_val: " + p_str)
#                     print("q_val: " + q_str)

# Distribution of diamond check -- full search space 

# for pol in polarities:
#     ops = all_ops(pol.attrs,pol.objs)    
#     ops.remove(set())
#     for rel in ops:
#         Rdia = BinaryRelation(pol.attrs,pol.objs,rel)        
#         for p_val in get_valuation_extents(pol):
#             p_descr = pol.up(p_val)
#             for q_val in get_valuation_extents(pol):
#                 q_descr = pol.up(q_val)
#                 lhs = pol.downup(Rdia.back(pol.down(p_descr.intersection(q_descr))))
#                 rhs = pol.downup(Rdia.back(p_val)).intersection(pol.downup(Rdia.back(q_val)))        
#                 if (lhs >= rhs) == False:
#                     P_str = pol.to_string()
#                     R_str = Rdia.to_string()
#                     p_str = str(p_val)
#                     q_str = str(q_val)
#                     print("P: " + P_str)
#                     print("Rdia: " + R_str)
#                     print("p_val: " + p_str)
#                     print("q_val: " + q_str)

# Distribution of diamond restricted to rotations of our box examples
polarities = [Polarity({x,y,z},{a,b},{(x,a),(z,b)})]

for pol in polarities:
    ops = all_ops(pol.attrs,pol.objs)    
    ops.remove(set())
    Rdia = BinaryRelation(pol.attrs,pol.objs,{(b,x),(b,z)})        
    for p_val in get_valuation_extents(pol):
        p_descr = pol.up(p_val)
        for q_val in get_valuation_extents(pol):
            q_descr = pol.up(q_val)
            lhs = pol.downup(Rdia.back(pol.down(p_descr.intersection(q_descr))))
            rhs = pol.downup(Rdia.back(p_val)).intersection(pol.downup(Rdia.back(q_val)))
            if (lhs >= rhs) == False:
                P_str = pol.to_string()
                R_str = Rdia.to_string()
                p_str = str(p_val)
                q_str = str(q_val)
                print("P: " + P_str)
                print("Rdia: " + R_str)
                print("p_val: " + p_str)
                print("q_val: " + q_str)

