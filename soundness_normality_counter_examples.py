from polarity import *

# a,b,c,d,w,x,y,z = "a", "b", "c", "d","w", "x", "y", "z"

# object_set = {a,b}
# feature_set = {x,y}

# # Soundness counter examples for \( \top \vdash \square \top \). We want to find \( \left( R_{\square} ^{(0)} \left( A ^ \uparrow  \right)   \right) ^\uparrow \supsetneq \left( A \right) ^\uparrow \)

# # Generating lists of non-empty subsets of object and feature sets. 

# object_sets = []
# for subset in powerset(object_set):
#     if subset != set():
#         object_sets.append(subset)

# feature_sets = []
# for subset in powerset(feature_set):
#     if subset != set():
#         feature_sets.append(subset)

# polarities = []
# n = 0

# for object_set in object_sets:
#     for feature_set in feature_sets:
#         for I in all_ops(object_set,feature_set): 
#             polarity = Polarity(object_set,feature_set,I)
#             for op in all_ops(object_set,feature_set): 
#                 Rbox = BinaryRelation(object_set,feature_set,op)
#                 if object_set  polarity.down(polarity.up(Rbox.back(polarity.up(object_set)))):
#                     print(object_set,feature_set,I,op)


# Soundness examples check for right triangle

# a,b,c,d,w,x,y,z = "a", "b", "c", "d","w", "x", "y", "z"

# object_set = {a,b}
# feature_set = {x,y}

# object_sets = []
# for subset in powerset(object_set):
#     if subset != set():
#         object_sets.append(subset)

# feature_sets = []
# for subset in powerset(feature_set):
#     if subset != set():
#         feature_sets.append(subset)

# polarities = []

# for object_set in object_sets:
#     for feature_set in feature_sets:
#         for I in all_ops(object_set,feature_set): 
#             polarity = Polarity(object_set,feature_set,I)
#             for op in all_ops(object_set,object_set): 
#                 Rrhd = BinaryRelation(object_set,object_set,op)
#                 if object_set != polarity.down(polarity.up(Rrhd.back(polarity.down(feature_set)))):
#                     print(object_set,feature_set,I,op)

# Checking lhd 

# a,b,c,d,w,x,y,z = "a", "b", "c", "d","w", "x", "y", "z"

# object_set = {a,b}
# feature_set = {x,y}

# object_sets = []
# for subset in powerset(object_set):
#     if subset != set():
#         object_sets.append(subset)

# feature_sets = []
# for subset in powerset(feature_set):
#     if subset != set():
#         feature_sets.append(subset)

# polarities = []

# for object_set in object_sets:
#     for feature_set in feature_sets:
#         for I in all_ops(object_set,feature_set): 
#             polarity = Polarity(object_set,feature_set,I)
#             for op in all_ops(feature_set,feature_set): 
#                 Rlhd = BinaryRelation(feature_set,feature_set,op)
#                 if feature_set != polarity.up(polarity.down(Rlhd.back(polarity.up(object_set)))):
#                     print(object_set,feature_set,I,op)

# Checking lhd 

# a,b,c,d,w,x,y,z = "a", "b", "c", "d","w", "x", "y", "z"

# object_set = {a,b,c,d}
# feature_set = {w,x,y,z}

# object_sets = []
# for subset in powerset(object_set):
#     if subset != set():
#         object_sets.append(subset)

# feature_sets = []
# for subset in powerset(feature_set):
#     if subset != set():
#         feature_sets.append(subset)

# for A in object_sets:
#     for X in feature_sets:
#         for I in all_ops(A,X):
#             P = Polarity(A,X,I)

# # left hand side of sequent \(\square p \wedge \square q\)
# # P is a polarity, and valuation_<p,q> is a subset of objects

# def box_ext(P,R,psi_ext):
#     psi_int = P.up(psi_ext)
# #    print(P.objs,P.attrs,P.I,R.ops,psi_ext, psi_int)
# #    print(R.back(psi_int))
#     return(P.down(P.up(R.back(psi_int))))
# counter = 0

# for object_set in object_sets:
#     val_p_extensions = [extension for extension in powerset(object_set)]
#     val_q_extensions = val_p_extensions
#     counter +=1
#     print(counter/len(object_sets))
#     for feature_set in feature_sets:
#         for I in all_ops(object_set,feature_set): 
#             polarity = Polarity(object_set,feature_set,I)
#             for op in all_ops(object_set,feature_set):
#                 Rbox = BinaryRelation(object_set,feature_set,op)
#                 for val_p in val_p_extensions:
#                     box_p_ext = box_ext(polarity,Rbox,val_p)
#                     for val_q in val_q_extensions:
#                         box_q_ext = box_ext(polarity,Rbox,val_q)
#                         box_p_inter_box_q = box_p_ext.intersection(box_q_ext)
#                         p_inter_q = val_p.intersection(val_q)
#                         box_p_inter_q = box_ext(polarity,Rbox,p_inter_q)
#                         if (box_p_inter_box_q == box_p_inter_box_q) == False:
#                             print(object_set,feature_set,I,op,val_p,val_q)                        
